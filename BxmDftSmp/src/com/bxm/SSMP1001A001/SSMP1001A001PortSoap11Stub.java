/**
 * SSMP1001A001PortSoap11Stub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class SSMP1001A001PortSoap11Stub extends org.apache.axis.client.Stub implements com.bxm.SSMP1001A001.SSMP1001A001Port {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[5];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ssmp1001a004");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a004Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a004Request"), com.bxm.SSMP1001A001.Ssmp1001A004Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a004Response"));
        oper.setReturnClass(com.bxm.SSMP1001A001.Ssmp1001A004Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a004Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ssmp1001a001");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a001Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a001Request"), com.bxm.SSMP1001A001.Ssmp1001A001Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a001Response"));
        oper.setReturnClass(com.bxm.SSMP1001A001.Ssmp1001A001Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a001Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ssmp1001a002");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a002Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a002Request"), com.bxm.SSMP1001A001.Ssmp1001A002Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a002Response"));
        oper.setReturnClass(com.bxm.SSMP1001A001.Ssmp1001A002Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a002Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ssmp1001a005");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a005Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a005Request"), com.bxm.SSMP1001A001.Ssmp1001A005Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a005Response"));
        oper.setReturnClass(com.bxm.SSMP1001A001.Ssmp1001A005Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a005Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ssmp1001a003");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a003Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a003Request"), com.bxm.SSMP1001A001.Ssmp1001A003Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a003Response"));
        oper.setReturnClass(com.bxm.SSMP1001A001.Ssmp1001A003Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "ssmp1001a003Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

    }

    public SSMP1001A001PortSoap11Stub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public SSMP1001A001PortSoap11Stub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public SSMP1001A001PortSoap11Stub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a001Request");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A001Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a001Response");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A001Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a002Request");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A002Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a002Response");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A002Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a003Request");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A003Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a003Response");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A003Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a004Request");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A004Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a004Response");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A004Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a005Request");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A005Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a005Response");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.Ssmp1001A005Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DefaultMessage");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.DefaultMessage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DefaultSystemHeader");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.DefaultSystemHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DSmpEmpTst000Dto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.DSmpEmpTst000Dto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A001InDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A001InDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A001OutDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A001OutDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A002InDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A002InDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A002OutDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A002OutDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A003InDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A003InDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A003OutDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A003OutDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A004InDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A004InDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A004OutDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A004OutDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A005InDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A005InDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A005OutDto");
            cachedSerQNames.add(qName);
            cls = com.bxm.SSMP1001A001.SSMP1001A005OutDto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.bxm.SSMP1001A001.Ssmp1001A004Response ssmp1001A004(com.bxm.SSMP1001A001.Ssmp1001A004Request ssmp1001A004Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ssmp1001a004"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ssmp1001A004Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bxm.SSMP1001A001.Ssmp1001A004Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bxm.SSMP1001A001.Ssmp1001A004Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.bxm.SSMP1001A001.Ssmp1001A004Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bxm.SSMP1001A001.Ssmp1001A001Response ssmp1001A001(com.bxm.SSMP1001A001.Ssmp1001A001Request ssmp1001A001Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ssmp1001a001"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ssmp1001A001Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bxm.SSMP1001A001.Ssmp1001A001Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bxm.SSMP1001A001.Ssmp1001A001Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.bxm.SSMP1001A001.Ssmp1001A001Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bxm.SSMP1001A001.Ssmp1001A002Response ssmp1001A002(com.bxm.SSMP1001A001.Ssmp1001A002Request ssmp1001A002Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ssmp1001a002"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ssmp1001A002Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bxm.SSMP1001A001.Ssmp1001A002Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bxm.SSMP1001A001.Ssmp1001A002Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.bxm.SSMP1001A001.Ssmp1001A002Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bxm.SSMP1001A001.Ssmp1001A005Response ssmp1001A005(com.bxm.SSMP1001A001.Ssmp1001A005Request ssmp1001A005Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ssmp1001a005"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ssmp1001A005Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bxm.SSMP1001A001.Ssmp1001A005Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bxm.SSMP1001A001.Ssmp1001A005Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.bxm.SSMP1001A001.Ssmp1001A005Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bxm.SSMP1001A001.Ssmp1001A003Response ssmp1001A003(com.bxm.SSMP1001A001.Ssmp1001A003Request ssmp1001A003Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ssmp1001a003"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ssmp1001A003Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bxm.SSMP1001A001.Ssmp1001A003Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bxm.SSMP1001A001.Ssmp1001A003Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.bxm.SSMP1001A001.Ssmp1001A003Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
