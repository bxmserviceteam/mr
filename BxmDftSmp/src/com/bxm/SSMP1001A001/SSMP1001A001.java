/**
 * SSMP1001A001.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public interface SSMP1001A001 extends javax.xml.rpc.Service {
    public java.lang.String getSSMP1001A001PortSoap11Address();

    public com.bxm.SSMP1001A001.SSMP1001A001Port getSSMP1001A001PortSoap11() throws javax.xml.rpc.ServiceException;

    public com.bxm.SSMP1001A001.SSMP1001A001Port getSSMP1001A001PortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
