/**
 * SSMP1001A001OutDto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class SSMP1001A001OutDto  implements java.io.Serializable {
    private java.lang.Integer feduEmpNo;

    private java.lang.String feduEmpNm;

    private java.lang.String feduOccpNm;

    private java.lang.Integer feduMngrEmpNo;

    private java.lang.String feduIpsaDt;

    private java.math.BigDecimal feduPayAmt;

    private java.lang.Integer feduDeptNo;

    public SSMP1001A001OutDto() {
    }

    public SSMP1001A001OutDto(
           java.lang.Integer feduEmpNo,
           java.lang.String feduEmpNm,
           java.lang.String feduOccpNm,
           java.lang.Integer feduMngrEmpNo,
           java.lang.String feduIpsaDt,
           java.math.BigDecimal feduPayAmt,
           java.lang.Integer feduDeptNo) {
           this.feduEmpNo = feduEmpNo;
           this.feduEmpNm = feduEmpNm;
           this.feduOccpNm = feduOccpNm;
           this.feduMngrEmpNo = feduMngrEmpNo;
           this.feduIpsaDt = feduIpsaDt;
           this.feduPayAmt = feduPayAmt;
           this.feduDeptNo = feduDeptNo;
    }


    /**
     * Gets the feduEmpNo value for this SSMP1001A001OutDto.
     * 
     * @return feduEmpNo
     */
    public java.lang.Integer getFeduEmpNo() {
        return feduEmpNo;
    }


    /**
     * Sets the feduEmpNo value for this SSMP1001A001OutDto.
     * 
     * @param feduEmpNo
     */
    public void setFeduEmpNo(java.lang.Integer feduEmpNo) {
        this.feduEmpNo = feduEmpNo;
    }


    /**
     * Gets the feduEmpNm value for this SSMP1001A001OutDto.
     * 
     * @return feduEmpNm
     */
    public java.lang.String getFeduEmpNm() {
        return feduEmpNm;
    }


    /**
     * Sets the feduEmpNm value for this SSMP1001A001OutDto.
     * 
     * @param feduEmpNm
     */
    public void setFeduEmpNm(java.lang.String feduEmpNm) {
        this.feduEmpNm = feduEmpNm;
    }


    /**
     * Gets the feduOccpNm value for this SSMP1001A001OutDto.
     * 
     * @return feduOccpNm
     */
    public java.lang.String getFeduOccpNm() {
        return feduOccpNm;
    }


    /**
     * Sets the feduOccpNm value for this SSMP1001A001OutDto.
     * 
     * @param feduOccpNm
     */
    public void setFeduOccpNm(java.lang.String feduOccpNm) {
        this.feduOccpNm = feduOccpNm;
    }


    /**
     * Gets the feduMngrEmpNo value for this SSMP1001A001OutDto.
     * 
     * @return feduMngrEmpNo
     */
    public java.lang.Integer getFeduMngrEmpNo() {
        return feduMngrEmpNo;
    }


    /**
     * Sets the feduMngrEmpNo value for this SSMP1001A001OutDto.
     * 
     * @param feduMngrEmpNo
     */
    public void setFeduMngrEmpNo(java.lang.Integer feduMngrEmpNo) {
        this.feduMngrEmpNo = feduMngrEmpNo;
    }


    /**
     * Gets the feduIpsaDt value for this SSMP1001A001OutDto.
     * 
     * @return feduIpsaDt
     */
    public java.lang.String getFeduIpsaDt() {
        return feduIpsaDt;
    }


    /**
     * Sets the feduIpsaDt value for this SSMP1001A001OutDto.
     * 
     * @param feduIpsaDt
     */
    public void setFeduIpsaDt(java.lang.String feduIpsaDt) {
        this.feduIpsaDt = feduIpsaDt;
    }


    /**
     * Gets the feduPayAmt value for this SSMP1001A001OutDto.
     * 
     * @return feduPayAmt
     */
    public java.math.BigDecimal getFeduPayAmt() {
        return feduPayAmt;
    }


    /**
     * Sets the feduPayAmt value for this SSMP1001A001OutDto.
     * 
     * @param feduPayAmt
     */
    public void setFeduPayAmt(java.math.BigDecimal feduPayAmt) {
        this.feduPayAmt = feduPayAmt;
    }


    /**
     * Gets the feduDeptNo value for this SSMP1001A001OutDto.
     * 
     * @return feduDeptNo
     */
    public java.lang.Integer getFeduDeptNo() {
        return feduDeptNo;
    }


    /**
     * Sets the feduDeptNo value for this SSMP1001A001OutDto.
     * 
     * @param feduDeptNo
     */
    public void setFeduDeptNo(java.lang.Integer feduDeptNo) {
        this.feduDeptNo = feduDeptNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSMP1001A001OutDto)) return false;
        SSMP1001A001OutDto other = (SSMP1001A001OutDto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.feduEmpNo==null && other.getFeduEmpNo()==null) || 
             (this.feduEmpNo!=null &&
              this.feduEmpNo.equals(other.getFeduEmpNo()))) &&
            ((this.feduEmpNm==null && other.getFeduEmpNm()==null) || 
             (this.feduEmpNm!=null &&
              this.feduEmpNm.equals(other.getFeduEmpNm()))) &&
            ((this.feduOccpNm==null && other.getFeduOccpNm()==null) || 
             (this.feduOccpNm!=null &&
              this.feduOccpNm.equals(other.getFeduOccpNm()))) &&
            ((this.feduMngrEmpNo==null && other.getFeduMngrEmpNo()==null) || 
             (this.feduMngrEmpNo!=null &&
              this.feduMngrEmpNo.equals(other.getFeduMngrEmpNo()))) &&
            ((this.feduIpsaDt==null && other.getFeduIpsaDt()==null) || 
             (this.feduIpsaDt!=null &&
              this.feduIpsaDt.equals(other.getFeduIpsaDt()))) &&
            ((this.feduPayAmt==null && other.getFeduPayAmt()==null) || 
             (this.feduPayAmt!=null &&
              this.feduPayAmt.equals(other.getFeduPayAmt()))) &&
            ((this.feduDeptNo==null && other.getFeduDeptNo()==null) || 
             (this.feduDeptNo!=null &&
              this.feduDeptNo.equals(other.getFeduDeptNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFeduEmpNo() != null) {
            _hashCode += getFeduEmpNo().hashCode();
        }
        if (getFeduEmpNm() != null) {
            _hashCode += getFeduEmpNm().hashCode();
        }
        if (getFeduOccpNm() != null) {
            _hashCode += getFeduOccpNm().hashCode();
        }
        if (getFeduMngrEmpNo() != null) {
            _hashCode += getFeduMngrEmpNo().hashCode();
        }
        if (getFeduIpsaDt() != null) {
            _hashCode += getFeduIpsaDt().hashCode();
        }
        if (getFeduPayAmt() != null) {
            _hashCode += getFeduPayAmt().hashCode();
        }
        if (getFeduDeptNo() != null) {
            _hashCode += getFeduDeptNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSMP1001A001OutDto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A001OutDto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduEmpNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduEmpNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduEmpNm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduEmpNm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduOccpNm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduOccpNm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduMngrEmpNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduMngrEmpNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduIpsaDt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduIpsaDt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduPayAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduPayAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduDeptNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduDeptNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
