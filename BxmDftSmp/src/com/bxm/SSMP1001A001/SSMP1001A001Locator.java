/**
 * SSMP1001A001Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class SSMP1001A001Locator extends org.apache.axis.client.Service implements com.bxm.SSMP1001A001.SSMP1001A001 {

    public SSMP1001A001Locator() {
    }


    public SSMP1001A001Locator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SSMP1001A001Locator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SSMP1001A001PortSoap11
    private java.lang.String SSMP1001A001PortSoap11_address = "http://52.79.125.85:8081/serviceEndpoint/webService/SSMP1001A001";

    public java.lang.String getSSMP1001A001PortSoap11Address() {
        return SSMP1001A001PortSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SSMP1001A001PortSoap11WSDDServiceName = "SSMP1001A001PortSoap11";

    public java.lang.String getSSMP1001A001PortSoap11WSDDServiceName() {
        return SSMP1001A001PortSoap11WSDDServiceName;
    }

    public void setSSMP1001A001PortSoap11WSDDServiceName(java.lang.String name) {
        SSMP1001A001PortSoap11WSDDServiceName = name;
    }

    public com.bxm.SSMP1001A001.SSMP1001A001Port getSSMP1001A001PortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SSMP1001A001PortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSSMP1001A001PortSoap11(endpoint);
    }

    public com.bxm.SSMP1001A001.SSMP1001A001Port getSSMP1001A001PortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bxm.SSMP1001A001.SSMP1001A001PortSoap11Stub _stub = new com.bxm.SSMP1001A001.SSMP1001A001PortSoap11Stub(portAddress, this);
            _stub.setPortName(getSSMP1001A001PortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSSMP1001A001PortSoap11EndpointAddress(java.lang.String address) {
        SSMP1001A001PortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bxm.SSMP1001A001.SSMP1001A001Port.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bxm.SSMP1001A001.SSMP1001A001PortSoap11Stub _stub = new com.bxm.SSMP1001A001.SSMP1001A001PortSoap11Stub(new java.net.URL(SSMP1001A001PortSoap11_address), this);
                _stub.setPortName(getSSMP1001A001PortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SSMP1001A001PortSoap11".equals(inputPortName)) {
            return getSSMP1001A001PortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A001");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A001PortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SSMP1001A001PortSoap11".equals(portName)) {
            setSSMP1001A001PortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
