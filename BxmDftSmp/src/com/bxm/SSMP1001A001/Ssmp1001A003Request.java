/**
 * Ssmp1001A003Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class Ssmp1001A003Request  implements java.io.Serializable {
    private com.bxm.SSMP1001A001.DefaultSystemHeader header;

    private com.bxm.SSMP1001A001.SSMP1001A003InDto SSMP1001A003InDto;

    public Ssmp1001A003Request() {
    }

    public Ssmp1001A003Request(
           com.bxm.SSMP1001A001.DefaultSystemHeader header,
           com.bxm.SSMP1001A001.SSMP1001A003InDto SSMP1001A003InDto) {
           this.header = header;
           this.SSMP1001A003InDto = SSMP1001A003InDto;
    }


    /**
     * Gets the header value for this Ssmp1001A003Request.
     * 
     * @return header
     */
    public com.bxm.SSMP1001A001.DefaultSystemHeader getHeader() {
        return header;
    }


    /**
     * Sets the header value for this Ssmp1001A003Request.
     * 
     * @param header
     */
    public void setHeader(com.bxm.SSMP1001A001.DefaultSystemHeader header) {
        this.header = header;
    }


    /**
     * Gets the SSMP1001A003InDto value for this Ssmp1001A003Request.
     * 
     * @return SSMP1001A003InDto
     */
    public com.bxm.SSMP1001A001.SSMP1001A003InDto getSSMP1001A003InDto() {
        return SSMP1001A003InDto;
    }


    /**
     * Sets the SSMP1001A003InDto value for this Ssmp1001A003Request.
     * 
     * @param SSMP1001A003InDto
     */
    public void setSSMP1001A003InDto(com.bxm.SSMP1001A001.SSMP1001A003InDto SSMP1001A003InDto) {
        this.SSMP1001A003InDto = SSMP1001A003InDto;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ssmp1001A003Request)) return false;
        Ssmp1001A003Request other = (Ssmp1001A003Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader()))) &&
            ((this.SSMP1001A003InDto==null && other.getSSMP1001A003InDto()==null) || 
             (this.SSMP1001A003InDto!=null &&
              this.SSMP1001A003InDto.equals(other.getSSMP1001A003InDto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        if (getSSMP1001A003InDto() != null) {
            _hashCode += getSSMP1001A003InDto().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ssmp1001A003Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", ">ssmp1001a003Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("", "header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DefaultSystemHeader"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSMP1001A003InDto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SSMP1001A003InDto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A003InDto"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
