/**
 * DefaultSystemHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class DefaultSystemHeader  implements java.io.Serializable {
    private java.lang.Long msgLen;

    private java.lang.String guid;

    private java.lang.String trxCd;

    private java.lang.String sysCd;

    private java.lang.String chlType;

    private java.lang.String clientIp;

    private java.lang.String msgReqDttm;

    private java.lang.String msgResDttm;

    private java.lang.String trxBranchNo;

    private java.lang.String trxTerminalNo;

    private java.lang.String userId;

    private java.lang.String apprTrxYn;

    private java.lang.String apprUserId;

    private java.lang.String screenId;

    private java.lang.String langCd;

    private java.lang.String resCd;

    private java.lang.String edayYn;

    private java.lang.String domainId;

    private java.lang.String filler;

    private java.lang.String msgCd;

    private java.lang.Integer msgCnt;

    private com.bxm.SSMP1001A001.DefaultMessage[] msgs;

    public DefaultSystemHeader() {
    }

    public DefaultSystemHeader(
           java.lang.Long msgLen,
           java.lang.String guid,
           java.lang.String trxCd,
           java.lang.String sysCd,
           java.lang.String chlType,
           java.lang.String clientIp,
           java.lang.String msgReqDttm,
           java.lang.String msgResDttm,
           java.lang.String trxBranchNo,
           java.lang.String trxTerminalNo,
           java.lang.String userId,
           java.lang.String apprTrxYn,
           java.lang.String apprUserId,
           java.lang.String screenId,
           java.lang.String langCd,
           java.lang.String resCd,
           java.lang.String edayYn,
           java.lang.String domainId,
           java.lang.String filler,
           java.lang.String msgCd,
           java.lang.Integer msgCnt,
           com.bxm.SSMP1001A001.DefaultMessage[] msgs) {
           this.msgLen = msgLen;
           this.guid = guid;
           this.trxCd = trxCd;
           this.sysCd = sysCd;
           this.chlType = chlType;
           this.clientIp = clientIp;
           this.msgReqDttm = msgReqDttm;
           this.msgResDttm = msgResDttm;
           this.trxBranchNo = trxBranchNo;
           this.trxTerminalNo = trxTerminalNo;
           this.userId = userId;
           this.apprTrxYn = apprTrxYn;
           this.apprUserId = apprUserId;
           this.screenId = screenId;
           this.langCd = langCd;
           this.resCd = resCd;
           this.edayYn = edayYn;
           this.domainId = domainId;
           this.filler = filler;
           this.msgCd = msgCd;
           this.msgCnt = msgCnt;
           this.msgs = msgs;
    }


    /**
     * Gets the msgLen value for this DefaultSystemHeader.
     * 
     * @return msgLen
     */
    public java.lang.Long getMsgLen() {
        return msgLen;
    }


    /**
     * Sets the msgLen value for this DefaultSystemHeader.
     * 
     * @param msgLen
     */
    public void setMsgLen(java.lang.Long msgLen) {
        this.msgLen = msgLen;
    }


    /**
     * Gets the guid value for this DefaultSystemHeader.
     * 
     * @return guid
     */
    public java.lang.String getGuid() {
        return guid;
    }


    /**
     * Sets the guid value for this DefaultSystemHeader.
     * 
     * @param guid
     */
    public void setGuid(java.lang.String guid) {
        this.guid = guid;
    }


    /**
     * Gets the trxCd value for this DefaultSystemHeader.
     * 
     * @return trxCd
     */
    public java.lang.String getTrxCd() {
        return trxCd;
    }


    /**
     * Sets the trxCd value for this DefaultSystemHeader.
     * 
     * @param trxCd
     */
    public void setTrxCd(java.lang.String trxCd) {
        this.trxCd = trxCd;
    }


    /**
     * Gets the sysCd value for this DefaultSystemHeader.
     * 
     * @return sysCd
     */
    public java.lang.String getSysCd() {
        return sysCd;
    }


    /**
     * Sets the sysCd value for this DefaultSystemHeader.
     * 
     * @param sysCd
     */
    public void setSysCd(java.lang.String sysCd) {
        this.sysCd = sysCd;
    }


    /**
     * Gets the chlType value for this DefaultSystemHeader.
     * 
     * @return chlType
     */
    public java.lang.String getChlType() {
        return chlType;
    }


    /**
     * Sets the chlType value for this DefaultSystemHeader.
     * 
     * @param chlType
     */
    public void setChlType(java.lang.String chlType) {
        this.chlType = chlType;
    }


    /**
     * Gets the clientIp value for this DefaultSystemHeader.
     * 
     * @return clientIp
     */
    public java.lang.String getClientIp() {
        return clientIp;
    }


    /**
     * Sets the clientIp value for this DefaultSystemHeader.
     * 
     * @param clientIp
     */
    public void setClientIp(java.lang.String clientIp) {
        this.clientIp = clientIp;
    }


    /**
     * Gets the msgReqDttm value for this DefaultSystemHeader.
     * 
     * @return msgReqDttm
     */
    public java.lang.String getMsgReqDttm() {
        return msgReqDttm;
    }


    /**
     * Sets the msgReqDttm value for this DefaultSystemHeader.
     * 
     * @param msgReqDttm
     */
    public void setMsgReqDttm(java.lang.String msgReqDttm) {
        this.msgReqDttm = msgReqDttm;
    }


    /**
     * Gets the msgResDttm value for this DefaultSystemHeader.
     * 
     * @return msgResDttm
     */
    public java.lang.String getMsgResDttm() {
        return msgResDttm;
    }


    /**
     * Sets the msgResDttm value for this DefaultSystemHeader.
     * 
     * @param msgResDttm
     */
    public void setMsgResDttm(java.lang.String msgResDttm) {
        this.msgResDttm = msgResDttm;
    }


    /**
     * Gets the trxBranchNo value for this DefaultSystemHeader.
     * 
     * @return trxBranchNo
     */
    public java.lang.String getTrxBranchNo() {
        return trxBranchNo;
    }


    /**
     * Sets the trxBranchNo value for this DefaultSystemHeader.
     * 
     * @param trxBranchNo
     */
    public void setTrxBranchNo(java.lang.String trxBranchNo) {
        this.trxBranchNo = trxBranchNo;
    }


    /**
     * Gets the trxTerminalNo value for this DefaultSystemHeader.
     * 
     * @return trxTerminalNo
     */
    public java.lang.String getTrxTerminalNo() {
        return trxTerminalNo;
    }


    /**
     * Sets the trxTerminalNo value for this DefaultSystemHeader.
     * 
     * @param trxTerminalNo
     */
    public void setTrxTerminalNo(java.lang.String trxTerminalNo) {
        this.trxTerminalNo = trxTerminalNo;
    }


    /**
     * Gets the userId value for this DefaultSystemHeader.
     * 
     * @return userId
     */
    public java.lang.String getUserId() {
        return userId;
    }


    /**
     * Sets the userId value for this DefaultSystemHeader.
     * 
     * @param userId
     */
    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }


    /**
     * Gets the apprTrxYn value for this DefaultSystemHeader.
     * 
     * @return apprTrxYn
     */
    public java.lang.String getApprTrxYn() {
        return apprTrxYn;
    }


    /**
     * Sets the apprTrxYn value for this DefaultSystemHeader.
     * 
     * @param apprTrxYn
     */
    public void setApprTrxYn(java.lang.String apprTrxYn) {
        this.apprTrxYn = apprTrxYn;
    }


    /**
     * Gets the apprUserId value for this DefaultSystemHeader.
     * 
     * @return apprUserId
     */
    public java.lang.String getApprUserId() {
        return apprUserId;
    }


    /**
     * Sets the apprUserId value for this DefaultSystemHeader.
     * 
     * @param apprUserId
     */
    public void setApprUserId(java.lang.String apprUserId) {
        this.apprUserId = apprUserId;
    }


    /**
     * Gets the screenId value for this DefaultSystemHeader.
     * 
     * @return screenId
     */
    public java.lang.String getScreenId() {
        return screenId;
    }


    /**
     * Sets the screenId value for this DefaultSystemHeader.
     * 
     * @param screenId
     */
    public void setScreenId(java.lang.String screenId) {
        this.screenId = screenId;
    }


    /**
     * Gets the langCd value for this DefaultSystemHeader.
     * 
     * @return langCd
     */
    public java.lang.String getLangCd() {
        return langCd;
    }


    /**
     * Sets the langCd value for this DefaultSystemHeader.
     * 
     * @param langCd
     */
    public void setLangCd(java.lang.String langCd) {
        this.langCd = langCd;
    }


    /**
     * Gets the resCd value for this DefaultSystemHeader.
     * 
     * @return resCd
     */
    public java.lang.String getResCd() {
        return resCd;
    }


    /**
     * Sets the resCd value for this DefaultSystemHeader.
     * 
     * @param resCd
     */
    public void setResCd(java.lang.String resCd) {
        this.resCd = resCd;
    }


    /**
     * Gets the edayYn value for this DefaultSystemHeader.
     * 
     * @return edayYn
     */
    public java.lang.String getEdayYn() {
        return edayYn;
    }


    /**
     * Sets the edayYn value for this DefaultSystemHeader.
     * 
     * @param edayYn
     */
    public void setEdayYn(java.lang.String edayYn) {
        this.edayYn = edayYn;
    }


    /**
     * Gets the domainId value for this DefaultSystemHeader.
     * 
     * @return domainId
     */
    public java.lang.String getDomainId() {
        return domainId;
    }


    /**
     * Sets the domainId value for this DefaultSystemHeader.
     * 
     * @param domainId
     */
    public void setDomainId(java.lang.String domainId) {
        this.domainId = domainId;
    }


    /**
     * Gets the filler value for this DefaultSystemHeader.
     * 
     * @return filler
     */
    public java.lang.String getFiller() {
        return filler;
    }


    /**
     * Sets the filler value for this DefaultSystemHeader.
     * 
     * @param filler
     */
    public void setFiller(java.lang.String filler) {
        this.filler = filler;
    }


    /**
     * Gets the msgCd value for this DefaultSystemHeader.
     * 
     * @return msgCd
     */
    public java.lang.String getMsgCd() {
        return msgCd;
    }


    /**
     * Sets the msgCd value for this DefaultSystemHeader.
     * 
     * @param msgCd
     */
    public void setMsgCd(java.lang.String msgCd) {
        this.msgCd = msgCd;
    }


    /**
     * Gets the msgCnt value for this DefaultSystemHeader.
     * 
     * @return msgCnt
     */
    public java.lang.Integer getMsgCnt() {
        return msgCnt;
    }


    /**
     * Sets the msgCnt value for this DefaultSystemHeader.
     * 
     * @param msgCnt
     */
    public void setMsgCnt(java.lang.Integer msgCnt) {
        this.msgCnt = msgCnt;
    }


    /**
     * Gets the msgs value for this DefaultSystemHeader.
     * 
     * @return msgs
     */
    public com.bxm.SSMP1001A001.DefaultMessage[] getMsgs() {
        return msgs;
    }


    /**
     * Sets the msgs value for this DefaultSystemHeader.
     * 
     * @param msgs
     */
    public void setMsgs(com.bxm.SSMP1001A001.DefaultMessage[] msgs) {
        this.msgs = msgs;
    }

    public com.bxm.SSMP1001A001.DefaultMessage getMsgs(int i) {
        return this.msgs[i];
    }

    public void setMsgs(int i, com.bxm.SSMP1001A001.DefaultMessage _value) {
        this.msgs[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DefaultSystemHeader)) return false;
        DefaultSystemHeader other = (DefaultSystemHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.msgLen==null && other.getMsgLen()==null) || 
             (this.msgLen!=null &&
              this.msgLen.equals(other.getMsgLen()))) &&
            ((this.guid==null && other.getGuid()==null) || 
             (this.guid!=null &&
              this.guid.equals(other.getGuid()))) &&
            ((this.trxCd==null && other.getTrxCd()==null) || 
             (this.trxCd!=null &&
              this.trxCd.equals(other.getTrxCd()))) &&
            ((this.sysCd==null && other.getSysCd()==null) || 
             (this.sysCd!=null &&
              this.sysCd.equals(other.getSysCd()))) &&
            ((this.chlType==null && other.getChlType()==null) || 
             (this.chlType!=null &&
              this.chlType.equals(other.getChlType()))) &&
            ((this.clientIp==null && other.getClientIp()==null) || 
             (this.clientIp!=null &&
              this.clientIp.equals(other.getClientIp()))) &&
            ((this.msgReqDttm==null && other.getMsgReqDttm()==null) || 
             (this.msgReqDttm!=null &&
              this.msgReqDttm.equals(other.getMsgReqDttm()))) &&
            ((this.msgResDttm==null && other.getMsgResDttm()==null) || 
             (this.msgResDttm!=null &&
              this.msgResDttm.equals(other.getMsgResDttm()))) &&
            ((this.trxBranchNo==null && other.getTrxBranchNo()==null) || 
             (this.trxBranchNo!=null &&
              this.trxBranchNo.equals(other.getTrxBranchNo()))) &&
            ((this.trxTerminalNo==null && other.getTrxTerminalNo()==null) || 
             (this.trxTerminalNo!=null &&
              this.trxTerminalNo.equals(other.getTrxTerminalNo()))) &&
            ((this.userId==null && other.getUserId()==null) || 
             (this.userId!=null &&
              this.userId.equals(other.getUserId()))) &&
            ((this.apprTrxYn==null && other.getApprTrxYn()==null) || 
             (this.apprTrxYn!=null &&
              this.apprTrxYn.equals(other.getApprTrxYn()))) &&
            ((this.apprUserId==null && other.getApprUserId()==null) || 
             (this.apprUserId!=null &&
              this.apprUserId.equals(other.getApprUserId()))) &&
            ((this.screenId==null && other.getScreenId()==null) || 
             (this.screenId!=null &&
              this.screenId.equals(other.getScreenId()))) &&
            ((this.langCd==null && other.getLangCd()==null) || 
             (this.langCd!=null &&
              this.langCd.equals(other.getLangCd()))) &&
            ((this.resCd==null && other.getResCd()==null) || 
             (this.resCd!=null &&
              this.resCd.equals(other.getResCd()))) &&
            ((this.edayYn==null && other.getEdayYn()==null) || 
             (this.edayYn!=null &&
              this.edayYn.equals(other.getEdayYn()))) &&
            ((this.domainId==null && other.getDomainId()==null) || 
             (this.domainId!=null &&
              this.domainId.equals(other.getDomainId()))) &&
            ((this.filler==null && other.getFiller()==null) || 
             (this.filler!=null &&
              this.filler.equals(other.getFiller()))) &&
            ((this.msgCd==null && other.getMsgCd()==null) || 
             (this.msgCd!=null &&
              this.msgCd.equals(other.getMsgCd()))) &&
            ((this.msgCnt==null && other.getMsgCnt()==null) || 
             (this.msgCnt!=null &&
              this.msgCnt.equals(other.getMsgCnt()))) &&
            ((this.msgs==null && other.getMsgs()==null) || 
             (this.msgs!=null &&
              java.util.Arrays.equals(this.msgs, other.getMsgs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMsgLen() != null) {
            _hashCode += getMsgLen().hashCode();
        }
        if (getGuid() != null) {
            _hashCode += getGuid().hashCode();
        }
        if (getTrxCd() != null) {
            _hashCode += getTrxCd().hashCode();
        }
        if (getSysCd() != null) {
            _hashCode += getSysCd().hashCode();
        }
        if (getChlType() != null) {
            _hashCode += getChlType().hashCode();
        }
        if (getClientIp() != null) {
            _hashCode += getClientIp().hashCode();
        }
        if (getMsgReqDttm() != null) {
            _hashCode += getMsgReqDttm().hashCode();
        }
        if (getMsgResDttm() != null) {
            _hashCode += getMsgResDttm().hashCode();
        }
        if (getTrxBranchNo() != null) {
            _hashCode += getTrxBranchNo().hashCode();
        }
        if (getTrxTerminalNo() != null) {
            _hashCode += getTrxTerminalNo().hashCode();
        }
        if (getUserId() != null) {
            _hashCode += getUserId().hashCode();
        }
        if (getApprTrxYn() != null) {
            _hashCode += getApprTrxYn().hashCode();
        }
        if (getApprUserId() != null) {
            _hashCode += getApprUserId().hashCode();
        }
        if (getScreenId() != null) {
            _hashCode += getScreenId().hashCode();
        }
        if (getLangCd() != null) {
            _hashCode += getLangCd().hashCode();
        }
        if (getResCd() != null) {
            _hashCode += getResCd().hashCode();
        }
        if (getEdayYn() != null) {
            _hashCode += getEdayYn().hashCode();
        }
        if (getDomainId() != null) {
            _hashCode += getDomainId().hashCode();
        }
        if (getFiller() != null) {
            _hashCode += getFiller().hashCode();
        }
        if (getMsgCd() != null) {
            _hashCode += getMsgCd().hashCode();
        }
        if (getMsgCnt() != null) {
            _hashCode += getMsgCnt().hashCode();
        }
        if (getMsgs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMsgs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMsgs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DefaultSystemHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DefaultSystemHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgLen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgLen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("guid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "guid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trxCd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trxCd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sysCd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sysCd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chlType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "chlType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientIp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clientIp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgReqDttm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgReqDttm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgResDttm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgResDttm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trxBranchNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trxBranchNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trxTerminalNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trxTerminalNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apprTrxYn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apprTrxYn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apprUserId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apprUserId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("screenId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "screenId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("langCd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "langCd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resCd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resCd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edayYn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "edayYn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("domainId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "domainId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filler");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filler"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgCd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgCd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DefaultMessage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
