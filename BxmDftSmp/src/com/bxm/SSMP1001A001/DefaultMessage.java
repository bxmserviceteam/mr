/**
 * DefaultMessage.javadwdwdw
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class DefaultMessage  implements java.io.Serializable {
    private java.lang.String errInfo;

    private java.lang.String basicMsg;

    private java.lang.Integer detailMsgCnt;

    private java.lang.String[] detailMsgs;

    public DefaultMessage() {
    }

    public DefaultMessage(
           java.lang.String errInfo,
           java.lang.String basicMsg,
           java.lang.Integer detailMsgCnt,
           java.lang.String[] detailMsgs) {
           this.errInfo = errInfo;
           this.basicMsg = basicMsg;
           this.detailMsgCnt = detailMsgCnt;
           this.detailMsgs = detailMsgs;
    }


    /**
     * Gets the errInfo value for this DefaultMessage.
     * 
     * @return errInfo
     */
    public java.lang.String getErrInfo() {
        return errInfo;
    }


    /**
     * Sets the errInfo value for this DefaultMessage.
     * 
     * @param errInfo
     */
    public void setErrInfo(java.lang.String errInfo) {
        this.errInfo = errInfo;
    }


    /**
     * Gets the basicMsg value for this DefaultMessage.
     * 
     * @return basicMsg
     */
    public java.lang.String getBasicMsg() {
        return basicMsg;
    }


    /**
     * Sets the basicMsg value for this DefaultMessage.
     * 
     * @param basicMsg
     */
    public void setBasicMsg(java.lang.String basicMsg) {
        this.basicMsg = basicMsg;
    }


    /**
     * Gets the detailMsgCnt value for this DefaultMessage.
     * 
     * @return detailMsgCnt
     */
    public java.lang.Integer getDetailMsgCnt() {
        return detailMsgCnt;
    }


    /**
     * Sets the detailMsgCnt value for this DefaultMessage.
     * 
     * @param detailMsgCnt
     */
    public void setDetailMsgCnt(java.lang.Integer detailMsgCnt) {
        this.detailMsgCnt = detailMsgCnt;
    }


    /**
     * Gets the detailMsgs value for this DefaultMessage.
     * 
     * @return detailMsgs
     */
    public java.lang.String[] getDetailMsgs() {
        return detailMsgs;
    }


    /**
     * Sets the detailMsgs value for this DefaultMessage.
     * 
     * @param detailMsgs
     */
    public void setDetailMsgs(java.lang.String[] detailMsgs) {
        this.detailMsgs = detailMsgs;
    }

    public java.lang.String getDetailMsgs(int i) {
        return this.detailMsgs[i];
    }

    public void setDetailMsgs(int i, java.lang.String _value) {
        this.detailMsgs[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DefaultMessage)) return false;
        DefaultMessage other = (DefaultMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errInfo==null && other.getErrInfo()==null) || 
             (this.errInfo!=null &&
              this.errInfo.equals(other.getErrInfo()))) &&
            ((this.basicMsg==null && other.getBasicMsg()==null) || 
             (this.basicMsg!=null &&
              this.basicMsg.equals(other.getBasicMsg()))) &&
            ((this.detailMsgCnt==null && other.getDetailMsgCnt()==null) || 
             (this.detailMsgCnt!=null &&
              this.detailMsgCnt.equals(other.getDetailMsgCnt()))) &&
            ((this.detailMsgs==null && other.getDetailMsgs()==null) || 
             (this.detailMsgs!=null &&
              java.util.Arrays.equals(this.detailMsgs, other.getDetailMsgs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrInfo() != null) {
            _hashCode += getErrInfo().hashCode();
        }
        if (getBasicMsg() != null) {
            _hashCode += getBasicMsg().hashCode();
        }
        if (getDetailMsgCnt() != null) {
            _hashCode += getDetailMsgCnt().hashCode();
        }
        if (getDetailMsgs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetailMsgs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetailMsgs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DefaultMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "DefaultMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basicMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "basicMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detailMsgCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detailMsgCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detailMsgs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detailMsgs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
