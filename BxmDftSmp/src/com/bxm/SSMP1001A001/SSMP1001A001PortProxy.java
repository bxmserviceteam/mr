package com.bxm.SSMP1001A001;

public class SSMP1001A001PortProxy implements com.bxm.SSMP1001A001.SSMP1001A001Port {
  private String _endpoint = null;
  private com.bxm.SSMP1001A001.SSMP1001A001Port sSMP1001A001Port = null;
  
  public SSMP1001A001PortProxy() {
    _initSSMP1001A001PortProxy();
  }
  
  public SSMP1001A001PortProxy(String endpoint) {
    _endpoint = endpoint;
    _initSSMP1001A001PortProxy();
  }
  
  private void _initSSMP1001A001PortProxy() {
    try {
      sSMP1001A001Port = (new com.bxm.SSMP1001A001.SSMP1001A001Locator()).getSSMP1001A001PortSoap11();
      if (sSMP1001A001Port != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sSMP1001A001Port)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sSMP1001A001Port)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sSMP1001A001Port != null)
      ((javax.xml.rpc.Stub)sSMP1001A001Port)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bxm.SSMP1001A001.SSMP1001A001Port getSSMP1001A001Port() {
    if (sSMP1001A001Port == null)
      _initSSMP1001A001PortProxy();
    return sSMP1001A001Port;
  }
  
  public com.bxm.SSMP1001A001.Ssmp1001A004Response ssmp1001A004(com.bxm.SSMP1001A001.Ssmp1001A004Request ssmp1001A004Request) throws java.rmi.RemoteException{
    if (sSMP1001A001Port == null)
      _initSSMP1001A001PortProxy();
    return sSMP1001A001Port.ssmp1001A004(ssmp1001A004Request);
  }
  
  public com.bxm.SSMP1001A001.Ssmp1001A001Response ssmp1001A001(com.bxm.SSMP1001A001.Ssmp1001A001Request ssmp1001A001Request) throws java.rmi.RemoteException{
    if (sSMP1001A001Port == null)
      _initSSMP1001A001PortProxy();
    return sSMP1001A001Port.ssmp1001A001(ssmp1001A001Request);
  }
  
  public com.bxm.SSMP1001A001.Ssmp1001A002Response ssmp1001A002(com.bxm.SSMP1001A001.Ssmp1001A002Request ssmp1001A002Request) throws java.rmi.RemoteException{
    if (sSMP1001A001Port == null)
      _initSSMP1001A001PortProxy();
    return sSMP1001A001Port.ssmp1001A002(ssmp1001A002Request);
  }
  
  public com.bxm.SSMP1001A001.Ssmp1001A005Response ssmp1001A005(com.bxm.SSMP1001A001.Ssmp1001A005Request ssmp1001A005Request) throws java.rmi.RemoteException{
    if (sSMP1001A001Port == null)
      _initSSMP1001A001PortProxy();
    return sSMP1001A001Port.ssmp1001A005(ssmp1001A005Request);
  }
  
  public com.bxm.SSMP1001A001.Ssmp1001A003Response ssmp1001A003(com.bxm.SSMP1001A001.Ssmp1001A003Request ssmp1001A003Request) throws java.rmi.RemoteException{
    if (sSMP1001A001Port == null)
      _initSSMP1001A001PortProxy();
    return sSMP1001A001Port.ssmp1001A003(ssmp1001A003Request);
  }
  
  
}