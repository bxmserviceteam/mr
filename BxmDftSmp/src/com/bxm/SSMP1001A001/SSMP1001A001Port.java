/**
 * SSMP1001A001Port.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public interface SSMP1001A001Port extends java.rmi.Remote {
    public com.bxm.SSMP1001A001.Ssmp1001A004Response ssmp1001A004(com.bxm.SSMP1001A001.Ssmp1001A004Request ssmp1001A004Request) throws java.rmi.RemoteException;
    public com.bxm.SSMP1001A001.Ssmp1001A001Response ssmp1001A001(com.bxm.SSMP1001A001.Ssmp1001A001Request ssmp1001A001Request) throws java.rmi.RemoteException;
    public com.bxm.SSMP1001A001.Ssmp1001A002Response ssmp1001A002(com.bxm.SSMP1001A001.Ssmp1001A002Request ssmp1001A002Request) throws java.rmi.RemoteException;
    public com.bxm.SSMP1001A001.Ssmp1001A005Response ssmp1001A005(com.bxm.SSMP1001A001.Ssmp1001A005Request ssmp1001A005Request) throws java.rmi.RemoteException;
    public com.bxm.SSMP1001A001.Ssmp1001A003Response ssmp1001A003(com.bxm.SSMP1001A001.Ssmp1001A003Request ssmp1001A003Request) throws java.rmi.RemoteException;
}
