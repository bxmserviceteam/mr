/**
 * SSMP1001A004InDto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bxm.SSMP1001A001;

public class SSMP1001A004InDto  implements java.io.Serializable {
    private java.lang.Integer feduEmpNo;

    public SSMP1001A004InDto() {
    }

    public SSMP1001A004InDto(
           java.lang.Integer feduEmpNo) {
           this.feduEmpNo = feduEmpNo;
    }


    /**
     * Gets the feduEmpNo value for this SSMP1001A004InDto.
     * 
     * @return feduEmpNo
     */
    public java.lang.Integer getFeduEmpNo() {
        return feduEmpNo;
    }


    /**
     * Sets the feduEmpNo value for this SSMP1001A004InDto.
     * 
     * @param feduEmpNo
     */
    public void setFeduEmpNo(java.lang.Integer feduEmpNo) {
        this.feduEmpNo = feduEmpNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSMP1001A004InDto)) return false;
        SSMP1001A004InDto other = (SSMP1001A004InDto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.feduEmpNo==null && other.getFeduEmpNo()==null) || 
             (this.feduEmpNo!=null &&
              this.feduEmpNo.equals(other.getFeduEmpNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFeduEmpNo() != null) {
            _hashCode += getFeduEmpNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSMP1001A004InDto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bxm.com/SSMP1001A001", "SSMP1001A004InDto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feduEmpNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "feduEmpNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
